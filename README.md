# CICD contra Alwaysdata PHP

Integració continua o millor dit desplegament continu, un exemple de desplegament al VPS de Alwaysdata amb un entorn de programació PHP

# Procediment

1. Crear un repositori
2. Crear un site de tipus PHP a Alwaysdata
3. Crear un parell de claus publica/privada per a poder accedir al servidor sense autentificar-se
   1. Crear les claus al l'estació de treball amb ssh-keygen
   2. Passar la clau publica al servidor ssh
   3. Comprovar que desde l'estació de treball es pot accedir al VPS sense haver de posar el codi i el password
4. Posar la clau privada com a variable d'entorn de CI/CD del gitlab
5. Crear un fitxer .gitlab-ci.yml al teu repositori (Pots utilitzar el d'aquest repo com a plantilla, però s'ha d'adaptar)
6. Realitzar commits i push en local a la teva estació de treball i comprovar que els pipelines funcionen i que el projecte es desplega en l'entorn de producció

# Test
http://rafael-cuestas.alwaysdata.net/cicd-contra-alwaysdata-php/
